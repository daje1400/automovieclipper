package autoFilmKlippning;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;

import javafx.scene.layout.Border;

public class Window extends JFrame{
	
	JPanel panel, row1, row2, row3, row4, row5;
	JTextArea filNamn;
	JTextField timeInfo;
	JButton cut, chooseFile, addTime;
	JComboBox<String> timeListFrom;
	JComboBox<String> timeListTo;
	JFileChooser chooser;
	File video;
	public Window(){
		this.setSize(500, 500);
		this.setTitle("Videoklippning");
		this.setResizable(false);
		init();
		this.add(panel, BorderLayout.CENTER);
		this.setVisible(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	public void init(){
		panel = new JPanel(new GridBagLayout());
		panel.setBackground(Color.RED);
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.fill = GridBagConstraints.HORIZONTAL;
		
		chooseFile = new JButton("�ppna fil");
		chooseFile.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				fileChooser();				
			}
		});
	
		constraints.anchor = GridBagConstraints.NORTHWEST;
		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.gridwidth = 1;
		constraints.insets = new Insets(10, 10, 10, 10);
		panel.add(chooseFile, constraints);
		
		filNamn = new JTextArea();
		filNamn.setEditable(false);	
		filNamn.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 20));
		constraints = new GridBagConstraints();
		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.anchor = GridBagConstraints.NORTHWEST;
		constraints.insets = new Insets(10, 10, 10, 10);
		constraints.gridx = 0;
		constraints.gridy = 1;
		constraints.gridwidth = 7;
		constraints.weightx = 0.5;
		panel.add(filNamn, constraints);
		
		timeListFrom = new JComboBox<String>();
		timeListFrom.setBackground(Color.white);
		constraints = new GridBagConstraints();
		constraints.gridx = 0;
		constraints.gridy = 2;
		constraints.gridwidth = 2;
		
		constraints.ipadx = 130;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.anchor = GridBagConstraints.NORTHWEST;
		constraints.insets = new Insets(10, 10, 30, 10);
		panel.add(timeListFrom, constraints);
		
		
		timeListTo = new JComboBox<String>();
		timeListTo.setBackground(Color.white);
		constraints.gridx = 2;
		panel.add(timeListTo, constraints);
		
		addTime = new JButton("L�gg till tiden");
		addTime.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
		});;
		constraints.gridx = 4;
		constraints.ipadx = 0;
		panel.add(addTime, constraints);
		
		timeInfo = new JTextField();
		timeInfo.setEditable(false);
		constraints.gridx = 0;
		constraints.gridy = 4;
		constraints.weighty = 0.8;
		constraints.gridheight = 7;
		constraints.gridwidth = 6;
		constraints.ipady = 200;
		panel.add(timeInfo, constraints);
		
		cut = new JButton("Klipp");
		cut.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
							
			}
		});
		constraints = new GridBagConstraints();
		constraints.anchor = GridBagConstraints.SOUTHEAST;
		constraints.gridx = 5;
		constraints.gridy = 12;
		constraints.insets = new Insets(10, 10, 10, 10);
		panel.add(cut,constraints);	
		
		
	}

	public void fileChooser(){
		chooser = new JFileChooser();
		FileNameExtensionFilter filter = new FileNameExtensionFilter("VIDEO", "avi", "mp4", "mkv");
		chooser.setFileFilter(filter);
		int returnValue = chooser.showOpenDialog(this);
		if (returnValue == JFileChooser.APPROVE_OPTION){
			video = new File(chooser.getSelectedFile(), chooser.getSelectedFile().getName());
			filNamn.setText(video.getName());
			calculateVideoTime();
		 } else {
			 filNamn.setText("Fil kan inte �ppnas");
		 }
	}
	private void calculateVideoTime() {
		
	}
}
